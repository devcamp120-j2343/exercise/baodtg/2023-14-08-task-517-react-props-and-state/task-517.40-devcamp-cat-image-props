import { Component } from "react";
import meow from '../assets/images/cat.jpg'
class AnimalComponent extends Component {

    render() {
        let { kindProps,  } = this.props
        return (
            <>
            {kindProps == "cat" ? <img src={meow} alt="cat"></img> : <p >Meow not found :)</p>}
              
            </>
        )
    }
}
export default AnimalComponent