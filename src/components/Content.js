import { Component } from "react";
import AnimalComponent from "./Animal";

class Content extends  Component{
    constructor(props) {
        super(props);
        this.state = {
            kind : "cat",
          
        }
    }
    render() {
        
        return(
            <AnimalComponent kindProps={this.state.kind}/>
        )
    }
}
export default Content